// alert('Hello World');

// console.log("Hello World");
// console.error('Ini adalah sebuah error');
// console.warn('This is warning');

//VARIABELS LET, CONST

/* const age = 30;
age = 31;
console.log(age); */

// let score1;
// score1 = 10;
// console.log(score1);

// //const score2; //harus assign nek ora yo error
// // console.log(score2);
// const score3 = 10;
// console.log(score3);

/* const name = "John";
const age = 30;
const rating = 4.5;
const isCool = true;
const x = null;
const y = undefined;
let z;

console.log(typeof age); */

/* const name = 'John';
const age = 30;

//concatenation
console.log('Nami Kulo is' + name + 'and I am' + age);
//Teamplate String
const hello = `My name is ${name} and I am ${age}`;
console.log(hello);

const s = 'technology, computers, it, code';
console.log(s.length);
console.log(s.toUpperCase());
console.log(s.toLowerCase());
console.log(s.substring(0, 5));
console.log(s.substring(0, 5).toUpperCase());
console.log(s.split(',')); */

//Array itu variabel yg dapat hold multiple values
/* const numbers = new Array(1,2,3,4,5);
console.log(numbers);
const fruit = ['apple','banana','orange'];
fruit[3] = 'salak kecut'
console.log(fruit)
console.log(fruit[2])

fruit.push('pelem');
fruit.unshift('stroberipedes');
fruit.pop(); //hapus array index pertama
console.log(fruit);
console.log(Array.isArray('hello'));
console.log(fruit.indexOf("banana")); */


//array of object
/* const person = {
    firsName: 'John',
    lastName: 'Doe',
    age: 30,
    hobbies: ['renang', 'nonton', 'begadang'],
    address: {
        street: '50 main st',
        city: 'Boston',
        state: 'MA'
    }
}

console.log(person.firsName)
console.log(person.hobbies[2])
console.log(person.address.city)

const { firsName, lastName, address: {city} } = person; //ini itu destructuring
console.log(firsName);
console.log(city);  

person.email = 'john@bmail.kom';
console.log(person); */


/* const todos = [
    {
        id: 1,
        text : 'Take out trash',
        isCompleted: true
    },
    {
        id: 2,
        text : 'Meeting with boss',
        isCompleted: true
    },
    {
        id: 3,
        text : 'Turu glundang glundung',
        isCompleted: true
    },
] */


/* const todoJSON = JSON.stringify(todos);
console.log(todoJSON)

for(let i = 0; i <= 10; i++){
    console.log(`For Loop Number: ${i}`);
}

let i = 0;
while(i<10) {
    console.log(`While Loop Number: ${i}`);
    i++;
}

for( let todo of todos) {
    console.log(todo);
}

for(let i = 0; i < todos.length; i++){
    console.log(todos[i].text)
} */

/* todos.forEach(function(todo){
    console.log(todo.text);
})

const todoText = todos.map(function(todo){
    return todo.text;
})

const todoCompleted = todos.filter(function(todo){
    return  todo.isCompleted === true;
}).map(function(todo){
    return todo.text;
})

console.log(todoCompleted); */

/* const x = 4;
const y = 10;;

if(x === 10) {
    console.log('x is 10')
}else if(x>10){
    console.log('x is greater than 10');
}else {
    console.log('x is less than 10');
}

const color = x > 10 ? 'red' : 'blue';

console.log(color);

switch(color) {
    case 'red':
        console.log('color is red');
        break;
    case 'blue':
        console.log('color is blue');
        break;
    default:
        console.log('color is Not red or blue');
        break;
} */

/* function addNums(num1 = 1, num2 = 2){
    return num1 + num2;
}

const addNums2 = (num1 = 1, num2 = 2)=>{ 
    return num1 + num2;
}

const addNums3 = (num1 = 1, num2 =2) => num1 + num2;

console.log(addNums2(5, 5)); */

/* //constructor function
function Person(firstName, lastName, dob){
    this.firstName = firstName;
    this.lastName = lastName
    this.dob = new Date(dob);
}

Person.prototype.getBirthYear = function() {
    return this.dob.getFullYear();
}

Person.prototype.getFullName = function() {
    return `${this.firstName} ${this.lastName}`;
}

// Class
class Person {
    constructor(firstName, lastName, dob) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.dob = new Date(dob)
    }

    getBirthYear() {
        return this.dob.getFullYear();
    }

    getFullName() {
        return `${this.firstName} ${this.lastName}`;
    }
}

//Instantiate object
const person1 = new Person('John', 'Doe', '4-3-1980');
const person2 = new Person('Mary', 'Smith', '3-6-190');
 

console.log(person2.getFullName())
console.log(person1); */


//single selektor
/* console.log(document.getElementById('my-form'));
console.log(document.querySelector('h1')); */

//multiple element
/* console.log(document.querySelectorAll('.item'));
console.log(document.getElementsByClassName('item'));
console.log(document.getElementsByTagName('li'));

const items = document.querySelectorAll('.item');
items.forEach((item)=>console.log(item));

const ul = document.querySelector('.items');
// ul.remove();
// ul.lastElementChild.remove();
ul.firstElementChild.textContent= 'Hello';
ul.children[1].innerText = 'Brad';
ul.lastElementChild.innerHTML = '<h1>Hellloooo</h>';

const btn = document.querySelector('.btn');
btn.style.background = 'red'; */

/* const btn = document.querySelector('.btn');
btn.addEventListener('click', (e)=> {
    e.preventDefault();
    console.log('click');
    console.log(e.target.className);
    console.log(e.target.id);
    document.querySelector('#my-form').style.background = '#ccc';
    document.querySelector('body').classList.add('bg-dark');
    document.querySelector('.items').lastElementChild.innerHTML='<h1>Hello</h1>';
}) */

const myForm = document.querySelector('#my-form');
const nameInput = document.querySelector('#name');
const emailInput = document.querySelector('#email');
const msg = document.querySelector('.msg');
const userList = document.querySelector('#users');

myForm.addEventListener('submit', onSubmit);

function onSubmit(e) {
    e.preventDefault();

    if(nameInput.value === ''|| emailInput.value ===''){
        msg.classList.add('error');
        msg.innerHTML = 'Tolong ya isikan semua field';
        setTimeout(()=>msg.remove(), 3000);
    }else {
        const li = document.createElement('li');
        li.appendChild(document.createTextNode(`${nameInput.value} : ${emailInput.value}`));
        userList.appendChild(li);

        nameInput.value = '';
        emailInput.value = '';
    }
}